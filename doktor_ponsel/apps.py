from django.apps import AppConfig


class WeddingOrganizerConfig(AppConfig):
    name = 'wedding_organizer'
