from django.contrib import admin 
from .models import Pemesan

# Register your models here.

@admin.register(Pemesan)
class PemesanAdmin(admin.ModelAdmin):
	list_display = ['nama_pemesan','paket','tgl_input','user']
	list_filter = ['nama_pemesan','paket','user']
	search_fields = ['nama_pemesan','paket','user']