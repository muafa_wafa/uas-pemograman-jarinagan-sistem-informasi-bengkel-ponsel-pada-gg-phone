from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse #new
from django.utils import timezone
# Create your models here.

class Pemesan(models.Model):
	PAKET_CHOICES = (
		('vn','Aksesoris'),
		('an','HP'),
		('ov','Service'),
	)
	JK_CHOICES = (
		('l','Laki-laki'),
		('p','Perempuan'),
	)
	nama_pemesan = models.CharField('Nama Produk', max_length=50, null=False)
	email = models.CharField('Tipe', max_length=50, null=False)
	paket = models.CharField('Kategori',max_length=2, choices=PAKET_CHOICES)
	alamat = models.CharField('Keterangan', max_length=50, null=False)
	tgl_input = models.DateTimeField('Tgl. Penambahan', default=timezone.now)
	user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

	class Meta:
		ordering = ['-tgl_input']

	def __str__(self):
		return self.nama_pemesan

	def get_absolute_url(self):
		return reverse('home_page')
